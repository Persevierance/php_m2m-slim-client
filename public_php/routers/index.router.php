<?php
/**
 * index.router.php
 *
 * CTEC3110 Secure Web Applications M2M Connect Assignment
 *
 * @contributors : Persevierance Musaka  [p11279300]
 *                 Jai Somaiya           [P13235864]
 *                Parmveer Mondair      [P12209198]
 *
 *
 **/

$models_path = $app->config('models.path');
$views_path = $app->config('templates.path');
$controllers_path = $app->config('controllers.path');


require $models_path. '/index.model.php';
require $controllers_path. '/index.controller.php';



$app->get(
	'/',
	function () use ($app){
		$base = new \lib\MVCbase();
		if(isset($_SESSION['user_logged_in'])){ //User is logged in go to profile
			$base->customRedirect('profile');
		}
		$app->render('default/index.view.php');
	}
);