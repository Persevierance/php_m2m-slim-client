<?php
/**
 * profile.router.php
 *
 * CTEC3110 Secure Web Applications M2M Connect Assignment
 *
 * @contributors : Persevierance Musaka  [p11279300]
 *                 Jai Somaiya           [P13235864]
 *                Parmveer Mondair      [P12209198]
 *
 *
 **/

$models_path = $app->config('models.path');
$views_path = $app->config('templates.path');
$controllers_path = $app->config('controllers.path');

require $models_path . '/profile.model.php';
require $controllers_path . '/profile.controller.php';

//Get Global Functions
$base = new \lib\MVCbase();

//Initialise Model and Controller vars
$profileController = new \controllers\profileController();
$profileModel = new \models\profileModel();



	$app->get(
		'/profile',
		function() use ($app, $base, $profileController, $profileModel) {
			if(!$base->isLoggedIn()) {//Check if user is not logged in
				$app->flash('feedback',
					$base->alertDismissible('warning', 'Please Login To Access Profile or Register'));
				$base->customRedirect('login');
			} else {
				$connectToSoap = $profileController->connectToSoap();
				$inbox = count($profileController->getData());
				$userDetails = $profileModel->getUserDetails($_SESSION['user_email']);

				$data = [
					'user_first_name' => $userDetails->mm_user_first_name,
					'user_last_name' => $userDetails->mm_user_last_name,
					'user_email' => $userDetails->mm_user_email,
					'soap_connection' => $connectToSoap,
					'database_connection' => $base->dbh
				];

				$app->render('profile.view.php', ['inbox' => $inbox, 'data' => $data, 'page' => 'default']);
			}
		}

	);// Default Profile page

	$app->get(
		'/profile/archive',
		function() use ($app, $base, $profileController, $profileModel) {
			if(!$base->isLoggedIn()) {//Check if user is not logged in
				$app->flash('feedback',
					$base->alertDismissible('warning', 'Please Login To Access Profile or Register'));
				$base->customRedirect('login');
			} else {
				$connectToSoap = $profileController->connectToSoap();
				$inbox = count($profileController->getData());
				$userDetails = $profileModel->getUserDetails($_SESSION['user_email']);

				$data_to_view = [
					'user_first_name' => $userDetails->mm_user_first_name,
					'user_last_name' => $userDetails->mm_user_last_name,
					'user_email' => $userDetails->mm_user_email,
					'soap_connection' => $connectToSoap,
					'database_connection' => $base->dbh,
					'soap_data' => $base->soapDetails()
				];

				$feedbackList = array();

				$smsList = $profileController->machineSMSList();

				foreach($smsList as $sms){//Get Single sms from list

					if($profileController->cleanMachineSMS($sms)){//SMS is VALID

						if($profileModel->saveMachineSMS($sms)){//SMS Saved to database
							$feedback = ['type' => 'success', 'message' => $sms['date_time_recieved'] . ' Saved to Database'];
							array_push($feedbackList, $feedback);
						}else{//SMS not saved

							$feedback = ['type' => 'danger', 'message' => $sms['date_time_recieved'] . ' Error Message not saved'];
							array_push($feedbackList, $feedback);
						}
					}else{//SMS invalid
						$feedback = ['type' => 'danger', 'message' => $sms['date_time_recieved'] . ' Invalid SMS Format'];
						array_push($feedbackList, $feedback);
					}
				}
				
				$app->render('profile.view.php', ['inbox' => $inbox, 'data' => $data_to_view, 'page' => 'archive']);
			}
		}
	);
