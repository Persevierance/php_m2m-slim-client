<?php
/**
 * login.router.php
 *
 * CTEC3110 Secure Web Applications M2M Connect Assignment
 *
 * @contributors : Persevierance Musaka  [p11279300]
 *                 Jai Somaiya           [P13235864]
 *                Parmveer Mondair      [P12209198]
 *
 *
 **/

/**
 * initialise Model View Controller Paths
 */

$models_path = $app->config('models.path');
$views_path = $app->config('templates.path');
$controllers_path = $app->config('controllers.path');

require $models_path . '/login.model.php';
require $controllers_path . '/login.controller.php';

$loginController = new controllers\loginController();
$loginModel = new models\loginModel();

//Get Global Functions
$base = new \lib\MVCbase();

$app->get(
	'/login',
	function() use ($app, $base){
		if($base->isLoggedIn()){
			$base->customRedirect('profile');
		}else {
			$app->render('login.view.php');
		}
	}
);

$app->get(
	'/login/logout',
	function()use($app, $base, $loginController){
		if($loginController->logout()) {
			$base->customRedirect('');
		}

	}
);

$app->post(
	'/login',
	function() use($app, $loginController, $loginModel,$base){

		//initialise login model
		$model = new models\loginModel();

		//get posted Data
		$data = $app->request->post();

		//get the clean and feedback processed data
		$loginControllerResponseData = $loginController->checkLoginDataNotEmpty($data)['formData'];


		$objectLoginModelData = $model->getLoginObject($loginControllerResponseData);
		
		if(!$model->getLoginObject($loginControllerResponseData)){
			$app->flash('feedback', $base->alertDismissible('danger','Login Not sucessful : Invalid email or password'));
		}else{
			if($loginController->verifyPassword($loginControllerResponseData['password'], $objectLoginModelData->mm_user_password)){
				$loginController->doLogin($data['email']);
				$app->flash('feedback', $base->alertDismissible('success','Login Successful'));
				$base->customRedirect('profile');
			}
		}
		$app->render('login.view.php');
	}
);
