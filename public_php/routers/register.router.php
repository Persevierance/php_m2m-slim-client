<?php
/**
 * register.router.php
 *
 * CTEC3110 Secure Web Applications M2M Connect Assignment
 *
 * @contributors : Persevierance Musaka  [p11279300]
 *                 Jai Somaiya           [P13235864]
 *                 Parmveer Mondair      [p12209198]
 *
 *
 **/

/**
 * initialise Model View Controller Paths
 */

$models_path = $app->config('models.path');
$views_path = $app->config('templates.path');
$controllers_path = $app->config('controllers.path');


require $models_path. '/register.model.php';
require $controllers_path. '/register.controller.php';

$registerController = new controllers\RegisterController();
$registerModel = new models\registerModel();

$base = new \lib\MVCbase();

$app->get(
	'/register',
	function() use ($app){
		$app->render('register.view.php');
	}
);

$app->post(
	'/register',
	function() use ($app,$registerController, $registerModel, $base) {

		$model = new models\registerModel();

		//get posted form data
		$data = $app->request->post();

		//get cleaned data array
		$controllerResponseData = $registerController->cleanRegisterData($data)['formData'];

		//get data cleaning feedback from controller
		$controllerResponseError = $registerController->cleanRegisterData($data)['errorData'];

		//Check for any errors
		if($controllerResponseError == Null) {

			//Insert Data into database
			if($model->doRegisteration($controllerResponseData)){
				$app->flashNow('feedback', $base->alertDismissible('success','Registration Successfull'));
				$app->render('register.view.php');
			}else{
				$app->flashNow('feedback', $base->alertDismissible('danger','New Account not created: user name or email already exists please <a href="/login">login</a>'));
				$app->render('register.view.php');
			}
		}else { //there are errors in the data
			foreach($controllerResponseError as $error => $message){
				$app->flashNow($error,$base->alertDismissible('danger',$message));
			}
			$app->render('register.view.php');
		}
	}
);
