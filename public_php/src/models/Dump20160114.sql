-- MySQL dump 10.13  Distrib 5.7.9, for osx10.9 (x86_64)
--
-- Host: localhost    Database: m2mDB
-- ------------------------------------------------------
-- Server version	5.5.42

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `mm_machine_sms`
--

DROP TABLE IF EXISTS `mm_machine_sms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mm_machine_sms` (
  `sms_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `switch_one` int(11) NOT NULL DEFAULT '0',
  `switch_two` int(11) NOT NULL DEFAULT '0',
  `switch_three` int(11) NOT NULL DEFAULT '0',
  `switch_four` int(11) NOT NULL DEFAULT '0',
  `fan` int(11) NOT NULL DEFAULT '0',
  `heater` int(11) NOT NULL DEFAULT '0',
  `keypad` int(11) NOT NULL DEFAULT '0',
  `time_recieved` datetime DEFAULT NULL,
  PRIMARY KEY (`sms_id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mm_machine_sms`
--

LOCK TABLES `mm_machine_sms` WRITE;
/*!40000 ALTER TABLE `mm_machine_sms` DISABLE KEYS */;
INSERT INTO `mm_machine_sms` VALUES (1,1,0,1,0,1,0,0,'0000-00-00 00:00:00'),(2,1,0,1,0,1,0,0,'0000-00-00 00:00:00'),(3,1,0,1,0,1,0,0,'0000-00-00 00:00:00'),(4,1,0,1,0,1,0,0,'0000-00-00 00:00:00'),(5,1,0,1,0,1,0,0,'0000-00-00 00:00:00'),(6,1,0,1,0,1,0,0,'0000-00-00 00:00:00'),(7,1,0,1,0,1,0,0,'0000-00-00 00:00:00'),(8,1,0,1,0,1,0,0,'0000-00-00 00:00:00'),(9,1,0,1,0,1,0,0,'0000-00-00 00:00:00'),(10,1,0,1,0,1,0,0,'0000-00-00 00:00:00'),(11,1,0,1,0,1,0,0,'0000-00-00 00:00:00'),(12,1,0,1,0,1,0,0,'0000-00-00 00:00:00'),(13,1,0,1,0,1,0,0,'0000-00-00 00:00:00'),(14,1,0,1,0,1,0,0,'0000-00-00 00:00:00'),(15,1,0,1,0,1,0,0,'0000-00-00 00:00:00'),(16,1,0,1,0,1,0,0,'0000-00-00 00:00:00'),(17,1,0,1,0,1,0,0,'0000-00-00 00:00:00'),(18,1,0,1,0,1,0,0,'0000-00-00 00:00:00'),(19,1,0,1,0,1,0,0,'0000-00-00 00:00:00'),(20,1,0,1,0,1,0,0,'0000-00-00 00:00:00'),(21,1,0,1,0,1,0,0,'0000-00-00 00:00:00'),(22,1,0,1,0,1,0,0,'0000-00-00 00:00:00'),(23,1,0,1,0,1,0,0,'0000-00-00 00:00:00'),(24,1,0,1,0,1,0,0,'0000-00-00 00:00:00'),(25,1,0,1,0,1,0,0,'0000-00-00 00:00:00'),(26,1,0,1,0,1,0,0,'0000-00-00 00:00:00'),(27,1,0,1,0,1,0,0,'0000-00-00 00:00:00'),(28,1,0,1,0,1,0,0,'0000-00-00 00:00:00'),(29,1,0,1,0,1,0,0,'0000-00-00 00:00:00');
/*!40000 ALTER TABLE `mm_machine_sms` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mm_users`
--

DROP TABLE IF EXISTS `mm_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mm_users` (
  `mm_user_id` int(11) NOT NULL AUTO_INCREMENT,
  `mm_user_first_name` varchar(64) NOT NULL,
  `mm_user_last_name` varchar(64) NOT NULL,
  `mm_user_email` varchar(64) NOT NULL,
  `mm_user_password` varchar(254) NOT NULL,
  PRIMARY KEY (`mm_user_id`),
  UNIQUE KEY `mm_user_id_UNIQUE` (`mm_user_id`),
  UNIQUE KEY `mm_user_email_UNIQUE` (`mm_user_email`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mm_users`
--

LOCK TABLES `mm_users` WRITE;
/*!40000 ALTER TABLE `mm_users` DISABLE KEYS */;
INSERT INTO `mm_users` VALUES (7,'Parmveer','Mondair','psm35@hotmail.com','$2y$10$Icq8C7CX.DbPl3anpWMe3uaxgla.aJF3Up7EisITIlUkS18YuHYqu'),(12,'Jai','Somia','psm3512@hotmail.com','$2y$10$dyC/O57sOCcRDI5K6kXBHeRlVaxZ/f6YquzPuPYQcwtV/F15FNuN6'),(32,'Persevierance','Musaka','pmusaka@instantia.co.uk','$2y$10$TmBb7tcnLEUkKCYcf7IDWeioFykwvHGoaW9Y3n4C0ilcZIt0jg11W');
/*!40000 ALTER TABLE `mm_users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-01-14 15:57:08
