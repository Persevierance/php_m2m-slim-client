<?php
/**
 * login.model.php
 *
 * CTEC3110 Secure Web Applications M2M Connect Assignment
 *
 * @contributors : Persevierance Musaka  [p11279300]
 *                 Jai Somaiya           [P13235864]
 *                Parmveer Mondair      [P12209198]
 *
 *
 **/

namespace models;
use lib\MVCbase;
use PDO;

class profileModel {
	protected $base;

	function __construct() {
		$this->base = MVCbase::getInstance();
	}

	public function getUserDetails($email){
		$sql = ('SELECT mm_user_first_name, mm_user_last_name, mm_user_email FROM mm_users WHERE mm_user_email =:email');
		$stmt = $this->base->dbh->prepare($sql);
		$stmt->bindParam(':email', $email);

		$result = $stmt->execute();
		$result_row = $stmt->fetchObject();
		if($result){
			return $result_row;
		}else{
			print_r($stmt->errorInfo());
		}
	}

	public 	function saveMachineSMS($data){
		$sql = ('INSERT INTO mm_machine_sms (switch_one, switch_two, switch_three, switch_four, fan, heater, keypad, time_recieved) VALUES (:switchOne, :switchTwo, :switchThree, :switchFour, :fan, :heater, :keypad, :time_recieved)');
		$stmp = $this->base->dbh->prepare($sql);
		$stmp->bindParam(':switchOne', $data['switchOne'] , PDO::PARAM_INT);
		$stmp->bindParam(':switchTwo', $data['switchTwo'] , PDO::PARAM_INT);
		$stmp->bindParam(':switchThree', $data['switchThree'] , PDO::PARAM_INT);
		$stmp->bindParam(':switchFour', $data['switchFour'] , PDO::PARAM_INT);
		$stmp->bindParam(':fan', $data['fan'] , PDO::PARAM_INT);
		$stmp->bindParam(':heater', $data['heater'] , PDO::PARAM_INT);
		$stmp->bindParam(':keypad', $data['keypad'] , PDO::PARAM_INT);
		$stmp->bindParam(':time_recieved', $data['date_time_recieved']);

		$result = $stmp->execute();
		if($result) {
			return true;
		}else {
			return false;
			print_r($stmp->errorInfo());
		}
	}

}