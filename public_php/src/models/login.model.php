<?php
/**
 * login.model.php
 *
 * CTEC3110 Secure Web Applications M2M Connect Assignment
 *
 * @contributors : Persevierance Musaka  [p11279300]
 *                 Jai Somaiya           [P13235864]
 *                Parmveer Mondair      [P12209198]
 *
 *
 **/

namespace models;
use lib\MVCbase;
use PDO;

class loginModel{
	protected $base;

	function __construct() {
		$this->base = MVCbase::getInstance();
	}

	/**
	 * @param $data
	 * @output mixed
	 */
	public function getLoginObject($data){
		$sql = ('SELECT mm_user_password, mm_user_email FROM mm_users WHERE mm_user_email = :email');
		$stmt = $this->base->dbh->prepare($sql);
		$stmt->bindParam(':email', $data['email']);

		$result = $stmt->execute();
		$result_row = $stmt->fetchObject();

		if($result){
			return $result_row;
		}
		else {
			print_r($stmt->errorInfo());
			return false;
		}
	}
}