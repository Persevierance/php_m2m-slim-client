<?php
/**
 * register.model.php
 *
 * CTEC3110 Secure Web Applications M2M Connect Assignment
 *
 * @contributors : Persevierance Musaka  [p11279300]
 *                 Jai Somaiya           [P13235864]
 *                Parmveer Mondair      [P12209198]
 *
 *
 **/

namespace models;
use lib\MVCbase;
use PDO;

class registerModel{
	protected $base;

	function __construct() {
		$this->base = MVCbase::getInstance();
	}

	/**
	 * @param $data
	 * inputs an array and return a  boolean value on insert state
	 */
	public function doRegisteration($data){

		$sql = ('INSERT INTO mm_users (mm_user_first_name, mm_user_last_name, mm_user_email, mm_user_password)
				VALUES(:first_name, :last_name, :email, :password)');
		$stmt = $this->base->dbh->prepare($sql);
		$stmt->bindValue(':first_name', $data['firstname']);
		$stmt->bindValue(':last_name', $data['lastname']);
		$stmt->bindValue(':email', $data['email']);
		$stmt->bindValue(':password', $data['password']);

		$result = $stmt->execute();
		if($result) {
			return true;
		}else {
			return false;
		}
	}
}
