<?php
/**
 * register.controller.php
 *
 * CTEC3110 Secure Web Applications M2M Connect Assignment
 *
 * @contributors : Persevierance Musaka  [p11279300]
 *                 Jai Somaiya           [P13235864]
 *                Parmveer Mondair      [P12209198]
 *
 *
 **/

namespace controllers;
use Slim\Slim;
Use Slim\Middleware\SessionCookie;


class loginController{

	/**
	 * @param array()  $data
	 * @return boolean
	 *
	 */

	function checkLoginDataNotEmpty($data){
		$user_email = $data['email'];
		$user_password = $data['password'];

		if(empty($user_email) || (empty($user_password))){
		}else {
			if(filter_var($user_email, FILTER_VALIDATE_EMAIL)) {
				$user_email = filter_var($user_email, FILTER_SANITIZE_EMAIL);
			}
		}

		$cleanData = ['email'=>$user_email, 'password'=>$user_password ];
		return array('formData'=>$cleanData);
	}

	function verifyPassword($password,$hash){
		if(password_verify($password, $hash)){
			return true;
		}
		return false;
	}

	function doLogin($user_email){
		$_SESSION['user_logged_in'] = true;
		$_SESSION['user_email'] = $user_email;
	}


	function logout(){
		unset($_SESSION['user_logged_in']);
		unset($_SESSION['email']);

		if(!isset($_SESSION['user_logged_in']) || !isset($_SESSION['email'])){ //User is not logged in all sessions destroyed
			return true;
		}
		return false;
	}

}