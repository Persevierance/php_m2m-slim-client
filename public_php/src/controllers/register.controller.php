<?php
/**
 * register.controller.php
 *
 * CTEC3110 Secure Web Applications M2M Connect Assignment
 *
 * @contributors : Persevierance Musaka  [p11279300]
 *                 Jai Somaiya           [P13235864]
 *                Parmveer Mondair      [P12209198]
 *
 *
 **/

namespace controllers;
use Slim\Slim;
Use Slim\Middleware\SessionCookie;


class RegisterController{

	/**
	 * @param $data
	 * @return mixed
	 *
	 * input array form params
	 * filter values of form
	 * return array of sanitized data
	 */
	function cleanRegisterData($data){
		$errorData = array();

		$fnameErr = null;
		$lnameErr = null;
		$emailErr = null;

		$user_lastname =  $data['lastname'];
		$user_firstname = $data['firstname'];
		$user_email = $data['email'];
		$password = $data['password'];

		if(!empty($user_firstname) && ctype_alpha($user_firstname)){
			$user_firstname = filter_var($user_firstname, FILTER_SANITIZE_STRING);
		}else{
			 $fnameErr = 'First Name not allowed';
		}

		if(!empty($user_lastname) && ctype_alpha($user_lastname)){
			$user_lastname = filter_var($user_lastname, FILTER_SANITIZE_STRING);
		}else{
			$lnameErr = 'Last Name not allowed';
		}

		if(!empty($user_email) && filter_var($user_email,FILTER_VALIDATE_EMAIL)){
			$user_email = filter_var($user_email, FILTER_SANITIZE_EMAIL);
		}else{
			$emailErr = 'Email address not allowed';
		}

		if(!empty($password)) {
			$options = ['cost' => 15 ];
			$password = password_hash($password, PASSWORD_BCRYPT, $options);
		}

		$cleanData =  ['firstname' => $user_firstname, 'lastname'=>$user_lastname, 'email'=>$user_email, 'password'=>$password];
		if($emailErr != null && $fnameErr != null && $lnameErr != null) {
			$errorData = ['firstname' => $fnameErr, 'lastname' => $lnameErr, 'email' => $emailErr];
		}

		return array('formData' => $cleanData, 'errorData' => $errorData);
	}
}