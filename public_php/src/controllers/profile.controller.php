<?php
/**
 * profile.controller.php
 *
 * CTEC3110 Secure Web Applications M2M Connect Assignment
 *
 * @contributors : Persevierance Musaka  [p11279300]
 *                 Jai Somaiya           [P13235864]
 *                Parmveer Mondair      [P12209198]
 *
 *
 **/

namespace controllers;
use lib\MVCbase;
use Slim\Slim;
Use Slim\Middleware\SessionCookie;
USE SoapClient;
class profileController{


	function connectToSoap(){//Check Connection to SoapClient
		$base = new MVCbase();

		try {
			$client = new SoapClient("https://m2mconnect.ee.co.uk/orange-soap/services/MessageServiceByCountry?wsdl");
			$connect = true;
			$feedback = $base->alertDismissible('success', 'Connected to SoapClient');
		} catch
		(Exception $e) {
			$connect = false;
			$feedback = $base->alertDismissible('danger',"Exception: \n" . $e->getMessage() . "\n");
		}

		return array('client'=>$client,'connect'=>$connect, 'feedback'=>$feedback);
	}

	function readFromSoap(){
		$client = $this->connectToSoap()['client'];
		$result = $client->readMessages(
			'15PMusaka',
			'p11279300Pmusaka',
			100,
			'447434953656'
		);

		$smsList = array();

		foreach($result as $textData){
			$smsXML = simplexml_load_string($textData);
			$dateRevieced = $smsXML->receivedtime;
			$message = $smsXML->message;
			$messageFrom = $smsXML->sourcemsisdn;

			$textAttr = ['DateTime' => $dateRevieced, 'From'=>$messageFrom, 'TXT'=>$message];

			array_push($smsList, $textAttr);
		}

		return $smsList;
	}

	function getData(){
		$client = $this->connectToSoap()['client'];
		$result = $client->peekMessages(
			'15PMusaka',
			'p11279300Pmusaka',
			100,
			'447434953656'
		);

		$smsList = array();

		foreach($result as $textData){
			$smsXML = simplexml_load_string($textData);
			$dateRevieced = $smsXML->receivedtime;
			$message = $smsXML->message;
			$messageFrom = $smsXML->sourcemsisdn;

			$textAttr = ['DateTime' => $dateRevieced, 'From'=>$messageFrom, 'TXT'=>$message];

			array_push($smsList, $textAttr);
		}

		return $smsList;
	}

	function sendSMS($data, $soapDetail){
		$client = $this->connectToSoap();

		$client->sendMessage(
			$soapDetail['user'],
			$soapDetail['pwsd'],
			'447817814149',
			$data);
	}

	function machineSMSList(){
		$data = $this->getData();
		$textsList = array();

		foreach ($data as $text){
			$textArray =  explode('-',$text['TXT']);
			$textToSave = [
				'switchOne' => $textArray[0],
				'switchTwo' => $textArray[1],
				'switchThree' => $textArray[2],
				'switchFour' => $textArray[3],
				'fan'=> $textArray[4],
				'heater'=> $textArray[5],
				'keypad'=> $textArray[6],
				'date_time_recieved' =>  $text['DateTime'],
			];
			array_push($textsList, $textToSave);
		}
		return $textsList;
	}

	function cleanMachineSMS($SMS){
		if(// validate SMS format
			($SMS['switchOne'] > 1 || $SMS['switchOne'] < 0) ||
			($SMS['switchTwo'] > 1 || $SMS['switchTwo'] < 0) ||
			($SMS['switchThree'] > 1 || $SMS['switchThree'] < 0) ||
			($SMS['switchFour'] > 1 || $SMS['switchFour'] < 0) ||
			($SMS['fan'] > 1 || $SMS['fan'] < 0) ||
			($SMS['heater'] > 1 || $SMS['heater'] < 0) ||
			($SMS['keypad'] > 9 || $SMS['keypad'] < 0)

		){
			return false;
		}else{

			return true;
		}
	}
}