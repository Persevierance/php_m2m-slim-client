<?php
/**
 * header.default..php
 *
 * CTEC3110 Secure Web Applications M2M Connect Assignment
 *
 * @contributors : Persevierance Musaka  [p11279300]
 *                 Jai Somaiya           [P13235864]
 *                Parmveer Mondair      [P12209198]
 *
 *
 **/
?>

<!DOCTYPE html>
<html><!--START OF HTML DOCUMENT (CLOSE TAG IN FOOTER)-->
<head>
	<title>M2M CONNECT | CTEC3110 </title>
	<link rel="stylesheet" type="text/css" href="/asserts/css/style.css">
	<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
	<link rel="icon" href="/asserts/images/favicon.ico">
	<script src="/asserts/js/jquery.lib.min.js"></script>
	<script type="text/javascript" src="http://code.jquery.com/jquery-1.10.0.min.js"></script>

	<script src="/asserts/js/custom.js"></script>
</head>

<body><!--START OF site body (CLOSE TAG IN FOOTER)-->
	<div id="contentWraper">
		<div class="row">
			<div class="container">
				<div class="header">
					<h3 class="text-muted">eeM2M </h3>
					<hr />
				</div>
			</div><!--EOF .container-->
		</div><!--EOF .row-->
