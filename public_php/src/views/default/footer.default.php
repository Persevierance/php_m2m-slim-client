<?php
/**
 * footer.default.php
 *
 * CTEC3110 Secure Web Applications M2M Connect Assignment
 *
 * @contributors : Persevierance Musaka  [p11279300]
 *                 Jai Somaiya           [P13235864]
 *                Parmveer Mondair      [P12209198]
 *
 *
 **/
?>

		<div class="row">
			<div class="container">
				<div class="footer">

					<hr />

					<div class="row"><p class="text-center text-muted">DEVELOPED BY</p></div>

					<div class="row">
						<div class="column-4">
							<p class="text-italic text-muted text-center">Persevierance Musaka</p>
						</div>
						<div class="column-4">
							<p class="text-italic text-muted text-center">Parmveer Mondair</p>
						</div>
						<div class="column-4">
							<p class="text-italic text-muted text-center">Jai Somaiya</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div><!--EOF #contentWrapper-->
</body>
</html>

