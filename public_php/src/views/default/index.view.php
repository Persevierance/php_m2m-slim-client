<?php
/**
 * index.view.php
 *
 * CTEC3110 Secure Web Applications M2M Connect Assignment
 *
 * @contributors : Persevierance Musaka  [p11279300]
 *                 Jai Somaiya           [P13235864]
 *                Parmveer Mondair      [P12209198]
 *
 *
 **/

?>

<div class="row">

	<div class="container text-center">
		<div class="panel panel-body">
			<h1 class="text-center text-muted">M2M Connect </h1>
			<h3 class="text-center text-muted">SMS PROCESSING UNIT</h3>
		</div>

		<div class="col-sm-6">
			<div class="well well-lg row">
				<div class="col-sm-6 col-sm-offset-3">
					<div class="">
						<hr />
						<a class="btn btn-default btn-sm btn-block" href="/login">Login</a>
					</div>
				</div>
			</div>
		</div>
		<div class="col-sm-6">
			<div class="well well-lg row">
				<div class="col-sm-6 col-sm-offset-3">
					<div class="">
						<hr />
						<a class="btn btn-default btn-sm btn-block" href="/register">Register</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>