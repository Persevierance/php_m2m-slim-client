<?php
/**
 * profile.view.php
 *
 * CTEC3110 Secure Web Applications M2M Connect Assignment
 *
 * @contributors : Persevierance Musaka  [p11279300]
 *                 Jai Somaiya           [P13235864]
 *                Parmveer Mondair      [P12209198]
 *
 *
 **/

	$first_name = $data['user_first_name'];
	$last_name = $data['user_last_name'];
	$email = $data['user_email'];
	$soap_connection = $data['soap_connection'] ? '<i class="text-success">{{established}}</i>' : '<i class="text-warning">{{failed}}</i>';
	$database_connection = $data['database_connection'] ? '<i class="text-success">{{established}}</i>' : '<i class="text-warning">{{failed}}</i>';

	$default_html  = '
	<div class="panel panel-default">
		<div class="panel panel-header"><h3 class="text-muted">'.$first_name.' '.$last_name.'  welcome to ee sms processor</h3> </div>
		<div class="panel-body">
			<div class="row">
				<div class="col-sm-4">
					<h4> Connection Details </h4>
					<div class="well well-sm"><p class="text-center">Soap Client Connection '.$soap_connection.'</p></div>
					<div class="well well-sm"><p class="text-center">Database Connection '.$soap_connection.'</p></div>
				</div>
				<div class="col-sm-8">
					<h4> Group Members </h4>
					<ul class="list-group well well-sm">
						<li class="list-group-item">Pamveer Mondair</li>
						<li class="list-group-item">Jai Somaiya</li>
						<li class="list-group-item">Persevierance Musaka</li>
					</ul>
				</div>
			</div>
		</div>
	</div>';

?>

 <div class="row">
    <div class="container text-center">
        <div class="row text-center">
            <div id="profile">
				<div class="row">
					<a class="<?php if($page == 'default'){ echo 'disabled';}else{ echo '';}?>  btn btn-default btn-sm" href="/profile">HOME</a>
					<a class="<?php if($page == 'inbox'){ echo 'disabled';}else{echo '';}?>  btn btn-default btn-sm" href="/profile/inbox">INBOX <span class="badge"><?= $inbox ?></span></a>
					<a class="btn btn-default btn-sm" href="/profile/archive">SYNC SMS</a>
					<a class="" href="/login/logout"><button class="btn btn-warning btn-sm">LOGOUT</button></a>
				</div>
				<hr />
					<h2 class="text-center text-muted">HOME</h2>
				<hr />

	            <?=$flash['feedback']?>

	            <div class="">
		            <div class="row">
			            <div class="col-sm-3 text-left">

				            <ul class="list-group">
					            <li class="list-group-item"><span class="badge"> <?= $inbox ?> </span>Messages Inbox</li>
				            </ul>
			            </div>

			            <div class="col-sm-9">
							<?php
								if($page == 'default'){echo $default_html;}
								elseif($page == 'reports'){}
								elseif($page == 'archive'){
									$flash['feedback'];
								}
								elseif($page == 'settings'){}
								else {
									echo $default_html;
								}
				            ?>
			            </div>
		            </div>

	            </div>
			</div>
		</div>
	</div>
</div>



