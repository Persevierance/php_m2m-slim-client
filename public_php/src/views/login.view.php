<?php
/**
* login.view.php
*
* CTEC3110 Secure Web Applications M2M Connect Assignment
*
* @contributors : Persevierance Musaka  [p11279300]
*                 Jai Somaiya           [P13235864]
*                Parmveer Mondair      [P12209198]
*
*
**/

print '
<div class="row">
	<div class="container row-5 padding-top-2">

		<div class="col-sm-8 col-sm-offset-2">
			<div id="login">

						'.$flash['feedback'].'

				<h3 class="text-center text-muted">LOGIN</h3>
				<form class="form-horizontal" action="/login" method="post">

					<div class="form-group">
						<div class="col-sm-4"><label for="username">Username</label></div>
						<div class="col-sm-8"><input class="form-control" name="email" type="email" required="required" placeholder="Username or Email Address" /></div>
					</div>

					<div class="form-group">
						<div class="col-sm-4">
							<label for="Password">Password</label>
						</div>
						<div class="col-sm-8">
							<input class="form-control" name="password" type="password" required="required" placeholder="**********" />
						</div>
					</div>

					<div class="row form-group">
						<div class="col-sm-5 col-sm-offset-6 text-center padding">
							<button class="btn btn-default btn-block btn-sm text-center" type="submit" name="login">LOGIN</button>
						</div>
					</div>

					<div class="row form-group">
						<div class="col-sm-5 col-sm-offset-6 text-center padding">
							<p class="text-center text-muted">Do not have an account ? <a href="/register">Register</a></p>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
';

