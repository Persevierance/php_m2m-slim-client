<?php
/**
 * register.view.php
 *
 * CTEC3110 Secure Web Applications M2M Connect Assignment
 *
 * @contributors : Persevierance Musaka  [p11279300]
 *                 Jai Somaiya           [P13235864]
 *                Parmveer Mondair      [P12209198]
 *
 *
 **/
?>
	<div class="row">
		<div class="container padding-top-2">

			<div class="col-sm-8 col-sm-offset-2">
				<div id="login">
					<?= '<p class="text-info text-center">' . $flash['feedback'] . '</p>'; ?>

					<h3 class="text-center text-muted">REGISTER</h3>
					<form class="form-horizontal" action="/register" method="post">
						<div class="row form-group">
							<div class="col-sm-4">
								<label for="User First Name">First Name</label>
							</div><div class="col-sm-8">
								<input name="firstname" class="form-control" type="text" required="required" placeholder="First Name" />
								<p class="text-danger text-center"> <?= $flash['firstname'];?></p>
							</div>
						</div>

						<div class="row form-group">
							<div class="col-sm-4">
								<label for="User Last Name">Last Name</label>
							</div><div class="col-sm-8">
								<input name="lastname" class="form-control" type="text" required="required" placeholder="Last Name" />
								<p class="text-danger text-center"> <?= $flash['lastname']; ?></p>
							</div>
						</div>

						<div class="row form-group">
							<div class="col-sm-4">
								<label for="User-Email">Email Address</label>
							</div><div class="col-sm-8">
								<input name="email" type="email" class="form-control" required="required" placeholder="example@domain.com" />
								<p class="text-danger text-center"> <?=$flash['email'];?></p>
							</div>
						</div>

						<div class="row form-group">
							<div class="col-sm-4">
								<label for="Password">Password</label>
							</div><div class="col-sm-8">
								<input name="password" id="password" type="password" class="form-control" required="required" placeholder="**********" />
							</div>
						</div>

						<div class="row form-group">
							<div class="col-sm-4">
								<label for="Password">Confirm Password</label>
							</div>
							<div class="col-sm-8">
								<input name="confirm-password" id="confirm_password" type="password" class="form-control" required="required" placeholder="**********" onChange="checkPasswordMatch();"/>
								<div class="registrationFormAlert" id="divCheckPasswordMatch"></div>
							</div>
						</div>

						<div class="row">
							<div class="col-sm-4 col-sm-offset-4 text-center padding">
								<button id="registerSubmit" disabled="disabled" class="btn btn-default btn-block text-center" type="submit" name="register">REGISTER</button>
							</div>
						</div>

						<div class="row">
							<p class="text-muted text-center"> Already have an account ? <a href="/login"> Login </a></p>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>

