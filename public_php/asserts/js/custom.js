
function checkPasswordMatch() {
    var password = $("#password").val();
    var confirmPassword = $("#confirm_password").val();

    if (password != confirmPassword) {
        $("#divCheckPasswordMatch").html("<p class='text-danger text-center'>Passwords do not match!</p>");
    }else {
        $("#divCheckPasswordMatch").html("<p class='text-danger text-center'> Passwords match.</p>");
        $("button[id=registerSubmit]").removeAttr("disabled");
    }
};

$(document).ready(function($){
    $("#confirm_password").keyup(checkPasswordMatch);
});
