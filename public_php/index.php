<?php
	/**
	 * index.php
	 *
	 * CTEC3110 Secure Web Applications M2M Connect Assignment
	 *
	 * @contributors : Persevierance Musaka  [p11279300]
	 *                 Jai Somaiya           [P13235864]
	 *                Parmveer Mondair      [P12209198]
	 *
	 **/
	session_start();

	require '../includes/slim/Slim/Slim.php';
	require 'MVCbase/mvc.base.php';

	\Slim\Slim::registerAutoloader();

	/**
	 * Step 2: Instantiate a Slim application
	 *
	 * This example instantiates a Slim application using
	 * its default settings. However, you will usually configure
	 * your Slim application now by passing an associative array
	 * of setting names and values into the application constructor.
	 *
	 */

	$app = new \Slim\Slim( array(
		'mode' => 'development',
		'debug' => 'true',
		'controllers.path' => __DIR__ . '/src/controllers',
		'models.path' => __DIR__ . '/src/models',
		'templates.path' => __DIR__ . '/src/views',
	));

	// Automatically load router files
	$routers = glob('routers/*.router.php');

	foreach ($routers as $router) {
		require $router;
	}

	$app->render('default/header.default.php');
	$app->run();
	$app->render('default/footer.default.php');
