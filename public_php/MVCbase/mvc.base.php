<?php
namespace lib;
use DBH;

class MVCbase{
    public $dbh;

    private static $instance;


    function __construct(){
        $m_rdbms = 'mysql';
        $m_host = 'localhost';
        $m_port = '3306';
        $m_db_name = 'm2mDB';
        $m_host_name = $m_rdbms . ':host=' . $m_host . ';port=' . $m_port . ';dbname=' . $m_db_name;
        $m_user_name = 'root';
        $m_user_pswd = 'root';

        try{
            $this->dbh = new \PDO($m_host_name, $m_user_name, $m_user_pswd);
        }catch(PDOException $e){
            $this->feedback = "PDO database connection problem: " . $e->getMessage();
        }catch(Exception $e){
            $this->feedback = "General problem: " . $e->getMessage();
        }
    }

    public function soapDetails(){
        return ['user'=>'PMusaka', 'pswd'=>'p11279300Pmusaka', 'terminal'=>'447434953656'];
    }

    public static function getInstance() {
        if (!isset(self::$instance))
        {
            $object = __CLASS__;
            self::$instance = new $object;
        }
        return self::$instance;
    }


    public function customRedirect($url){
        echo '<script type="text/javascript">
            window.location ="';
        echo '/';
        echo   		$url;
        echo '"</script>';
    }

    public function isLoggedIn(){
        if(isset($_SESSION['user_email']) && isset($_SESSION['user_logged_in'])){
            return true;
        }
        return false;
    }

    public function alertDismissible($type, $message){
        return "<div role='alert' class='alert alert-dismissable alert-$type  text-center '>
				<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
				$message
			</div>";
    }

}

